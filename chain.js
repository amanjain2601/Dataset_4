const data = require("./data.js");


let answer = data.map((currentObject) => {
    let cloneCurrentObject = { ...currentObject }
    let balance = cloneCurrentObject.balance.slice(1).split(",").join("");

    if (isNaN(balance))
        cloneCurrentObject["corrected_Balance"] = 0;
    else
        cloneCurrentObject["corrected_Balance"] = parseFloat(balance);

    return cloneCurrentObject;

}).sort(function (a, b) {
    return b.age - a.age;
}).map((currentObject) => {

    let friendArray = currentObject.friends;

    let NamesArray = friendArray.map((current) => {
        return current.name;
    })

    currentObject.friends = NamesArray;

    return currentObject;

}).map((currentObject) => {

    delete currentObject.tags;

    return currentObject;

}).map((currentObject) => {

    //Get valid datestring from string
    let CurrentDateString = currentObject.registered.split(" ")[0];

    let currentDate = new Date(CurrentDateString);
    let date = currentDate.getDate().toString();
    let month = (currentDate.getMonth() + 1).toString();
    let year = currentDate.getFullYear().toString();

    currentObject.registered = date + "/" + month + "/" + year;

    return currentObject;

}).filter((currentObject) => {
    return currentObject.isActive

}).reduce((totalBalance, currentObject) => {
    return totalBalance + currentObject.corrected_Balance;
}, 0)


console.log(answer);
